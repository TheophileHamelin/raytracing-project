#pragma once

#include <vector>
#include <FreeImage.h>
#include "Object.h"
#include "LightSource.h"
#include "Camera.h"

#define SCREEN_WIDTH 720
#define SCREEN_HEIGHT 480

/**
 * Class representing a scene
 */
class Scene
{
private:
	/**
	 * Camera used
	 */
	Camera* m_camera;

	/**
	 * Width of the screen
	 */
	int m_screenWidth;

	/**
	 * Height of the screen
	 */
	int m_screenHeight;

	/**
	 * Pixels of the screen where the 3D objects will be printed.
	 */
	RGBQUAD m_screenPixels[SCREEN_WIDTH][SCREEN_HEIGHT]{};

	/**
	 * List of 3D objects in the current scene
	 */
	std::vector<Object*> m_objects;

	/**
	 * The light sources of the current scene
	 */
	std::vector<LightSource*> m_light_sources;

public:
	/**
	 * Constructor
	 * @param p_camera The camera used for the scene
	 */
	explicit Scene(Camera* p_camera, int p_screenWidth, int p_screenHeight);

	/**
	 * Adding a light source to the LightSource vector
	 * @param p_light_source The LightSource to add
	 */
	void addLight(LightSource* p_light_source);

	/**
	 * Adding an Object to the Object vector
	 * @param p_object The Object to add
	 */
	void addObject(Object* p_object);

	/**
	 * Compute the scene seen by the camera
	 */
	void render();

	/**
	 * Compute the render of an object according the the light sources and the other objects
	 * @param p_object The object to render
	 * @param p_ray The ray traced from the camera to a screen pixel
	 * @param p_lowest_distance The lowest distance between the camera and an object at the current pixel
	 * @param p_xScreenPixel X coordinate of the screen pixel
	 * @param p_yScreenPixel Y coordinate of the screen pixel
	 * @return True if the pixel must be rendered, False if it mustn't
	 */
	bool renderObject(Object* p_object, const Vec3& p_ray, double* p_lowest_distance, unsigned int p_xScreenPixel,
		unsigned int p_yScreenPixel);

	/**
	 * Save the current scene into a file
	 * @param p_filename The filepath where the scene should be saved
	 */
	void save(const char* p_filename);

	/**
	 * Anti-aliasing
	 */
	void blur();

	/**
	 * Destructor
	 */
	~Scene();
};