#include <cmath>
#include "Camera.h"
#include "MathVector.h"

#define PI 3.14159265358979323846


Camera::Camera(const Vec3& p_position, const Vec3& p_direction, double p_focal, int p_horizontalFOV, int p_verticalFOV,
	int p_screenWidth, int p_screenHeight)
{
	this->m_position = p_position;
	this->m_baseDirection = p_direction;
	this->m_focalDistance = p_focal;
	this->m_horizontalFOV = p_horizontalFOV;
	this->m_verticalFOV = p_verticalFOV;
	this->m_screenWidth = p_screenWidth;
	this->m_screenHeight = p_screenHeight;

	// Use tan to compute the half height of the projection screen, converting the angle into radians
	// , and multiplying it by 2 to have the full height

	double projectionHeight =
		std::tan((static_cast<float>(this->m_verticalFOV) / 2.0f) * (PI / 180.0f)) * 2 * this->m_focalDistance;

	// Same thing for the width

	double projectionWidth =
		std::tan((static_cast<float>(this->m_horizontalFOV) / 2.0f) * (PI / 180.0f)) * 2 * this->m_focalDistance;


	this->m_screenHorizontalScale = projectionWidth / static_cast<double>(p_screenWidth);
	this->m_screenVerticalScale = projectionHeight / static_cast<double>(p_screenHeight);

	this->m_pointingRay = MathVector::multiply(this->m_focalDistance, this->m_baseDirection);
}

Vec3 Camera::getRayForScreenPixel(int p_pixelX, int p_pixelY)
{
	double projectionPlaneX = (static_cast<double>(p_pixelX) - (static_cast<double>(this->m_screenWidth) / 2.0f)) *
		this->m_screenHorizontalScale;
	double projectionPlaneY = (static_cast<double>(p_pixelY) - (static_cast<double>(this->m_screenHeight) / 2.0f)) *
		this->m_screenVerticalScale;
	return MathVector::normalize((this->m_pointingRay + Vec3(projectionPlaneX, projectionPlaneY, 0)));
}

const Vec3& Camera::getMPosition() const
{
	return m_position;
}

Camera::~Camera() = default;
