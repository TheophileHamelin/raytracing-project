#pragma once

#include "Vec3.h"
#include "Triangle.h"

class Tetrahedron : public Object
{
private:
	/**
	 * The first face of the tetrahedron
	 */
	Triangle m_face1;

	/**
	 * The second face of the tetrahedron
	 */
	Triangle m_face2;

	/**
	 * The third face of the tetrahedron
	 */
	Triangle m_face3;

	/**
	 * The base of the tetrahedron
	 */
	Triangle m_base;

public:
	/**
	 * Constructor of the tetrahedron
	 * @param mColor The color of the tetrahedron
	 * @param pBrillianceAmount The brilliance amount of the tetrahedron
	 * @param mPoint1 The first vertex of the tetrahedron
	 * @param mPoint2 The second vertex of the tetrahedron
	 * @param mPoint3 The third vertex of the tetrahedron
	 * @param mPoint4 The fourth vertex of the tetrahedron
	 * @param p_reflect_coefficient the reflect coefficient
	 */
	Tetrahedron(const tagRGBQUAD& mColor, double pBrillianceAmount, const Vec3& mPoint1,
		const Vec3& mPoint2, const Vec3& mPoint3, const Vec3& mPoint4, double p_reflect_coefficient);

	/**
	 * Getter of the first face
	 * @return The first face
	 */
	[[nodiscard]] const Triangle& getMFace1() const;

	/**
	 * Getter of the second face
	 * @return The second face
	 */
	[[nodiscard]] const Triangle& getMFace2() const;

	/**
	 * Getter of the third face
	 * @return The third face
	 */
	[[nodiscard]] const Triangle& getMFace3() const;

	/**
	 * Getter of the base
	 * @return The base
	 */
	[[nodiscard]] const Triangle& getMBase() const;

	/**
	 * Destructor
	 */
	~Tetrahedron() override;

	/**
	 * Compute the centroid of the tetrahedron formed by the vertices in parameters
	 * @param mPoint1 The first vertex
	 * @param mPoint2 The second vertex
	 * @param mPoint3 The third vertex
	 * @param mPoint4 The fourth vertex
	 * @return The centroid position of the tetrahedron formed by the four vertices
	 */
	static Vec3 computeCentroid(const Vec3& mPoint1, const Vec3& mPoint2, const Vec3& mPoint3, const Vec3& mPoint4);

	/**
	 * Compute the normal at the vector in parameter for a tetrahedron
	 * @param p_point The point to compute the normal at
	 * @return The normal vector at the given vector
	 */
	Vec3 getNormalAt(Vec3 p_point) override;
};
