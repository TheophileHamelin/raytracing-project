#pragma once

#include "Object.h"

/**
 * Class representing a 3D sphere.
 * This class herits from the Object class
 */
class Sphere : public Object
{
protected:
	/**
	 * The radius of the sphere
	 */
	float m_radius;

public:
	/**
	 * Constructor
	 * @param mPosition the position of the sphere
	 * @param mColor the color of the sphere
	 * @param mRadius the radius of the sphere
	 * @param mBrillianceAmount The ammount of brillance of the sphere
	 * @param p_reflect_coefficient the reflect coefficient
	 */
	Sphere(const Vec3& mPosition, tagRGBQUAD mColor, float mRadius, double mBrillianceAmount,
		double p_reflect_coefficient);

	/**
	 * Getter of the radius
	 * @return The radius of the sphere
	 */
	[[nodiscard]] float getMRadius() const;

	/**
	 * Compute the normal at the vector in parameter for a sphere
	 * @param p_point The point to compute the normal at
	 * @return The normal vector at the given vector
	 */
	Vec3 getNormalAt(Vec3 p_point) override;

};