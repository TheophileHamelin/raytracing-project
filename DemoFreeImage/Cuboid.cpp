#include "Cuboid.h"
#include "MathVector.h"


Cuboid::Cuboid(const tagRGBQUAD& mColor, double pBrillianceAmount, const Vec3& mPoint1,
	const Vec3& mPoint2, const Vec3& mPoint3, const Vec3& mPoint4, const Vec3& mPoint5,
	const Vec3& mPoint6, const Vec3& mPoint7, const Vec3& mPoint8, double p_reflect_coefficient) : Object(
		Cuboid::computeCentroid(mPoint1, mPoint2, mPoint3, mPoint4, mPoint5, mPoint6, mPoint7, mPoint8), mColor,
		pBrillianceAmount, p_reflect_coefficient)
{
	this->m_face1 = Rectangle(mColor, mPoint1, mPoint2, mPoint3, mPoint4, pBrillianceAmount, p_reflect_coefficient);
	this->m_face2 = Rectangle(mColor, mPoint6, mPoint5, mPoint8, mPoint7, pBrillianceAmount, p_reflect_coefficient);
	this->m_face3 = Rectangle(mColor, mPoint1, mPoint5, mPoint6, mPoint2, pBrillianceAmount, p_reflect_coefficient);
	this->m_face4 = Rectangle(mColor, mPoint2, mPoint6, mPoint7, mPoint3, pBrillianceAmount, p_reflect_coefficient);
	this->m_face5 = Rectangle(mColor, mPoint3, mPoint7, mPoint8, mPoint4, pBrillianceAmount, p_reflect_coefficient);
	this->m_face6 = Rectangle(mColor, mPoint5, mPoint1, mPoint4, mPoint8, pBrillianceAmount, p_reflect_coefficient);
}

const Rectangle& Cuboid::getMFace1() const
{
	return m_face1;
}

const Rectangle& Cuboid::getMFace2() const
{
	return m_face2;
}

const Rectangle& Cuboid::getMFace3() const
{
	return m_face3;
}

const Rectangle& Cuboid::getMFace4() const
{
	return m_face4;
}

const Rectangle& Cuboid::getMFace5() const
{
	return m_face5;
}

const Rectangle& Cuboid::getMFace6() const
{
	return m_face6;
}

Cuboid::~Cuboid()
{

}

Vec3 Cuboid::computeCentroid(const Vec3& mPoint1, const Vec3& mPoint2, const Vec3& mPoint3, const Vec3& mPoint4,
	const Vec3& mPoint5, const Vec3& mPoint6, const Vec3& mPoint7, const Vec3& mPoint8)
{
	double x =
		(mPoint1.getCoordinateX() + mPoint2.getCoordinateX() + mPoint3.getCoordinateX() + mPoint4.getCoordinateX()
			+ mPoint5.getCoordinateX() + mPoint6.getCoordinateX() + mPoint7.getCoordinateX() +
			mPoint8.getCoordinateX()) / 8.0;
	double y =
		(mPoint1.getCoordinateY() + mPoint2.getCoordinateY() + mPoint3.getCoordinateY() + mPoint4.getCoordinateY()
			+ mPoint5.getCoordinateY() + mPoint6.getCoordinateY() + mPoint7.getCoordinateY() +
			mPoint8.getCoordinateY()) / 8.0;
	double z =
		(mPoint1.getCoordinateZ() + mPoint2.getCoordinateZ() + mPoint3.getCoordinateZ() + mPoint4.getCoordinateZ()
			+ mPoint5.getCoordinateZ() + mPoint6.getCoordinateZ() + mPoint7.getCoordinateZ() +
			mPoint8.getCoordinateZ()) / 8.0;

	return Vec3(x, y, z);
}

Vec3 Cuboid::getNormalAt(Vec3 p_point)
{
	return MathVector::normalize(p_point - this->getMPosition());
}
