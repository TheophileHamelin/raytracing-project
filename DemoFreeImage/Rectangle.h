#pragma once

#include "Vec3.h"
#include "Object.h"

/**
 * Class representing a rectangle, a 2D object
 */
class Rectangle : public Object
{
private:
	/**
	 * First vertex of the rectangle
	 */
	Vec3 m_point1;
	/**
	 * Second vertex of the rectangle
	 */
	Vec3 m_point2;
	/**
	 * Third vertex of the rectangle
	 */
	Vec3 m_point3;
	/**
	 * 4th vertex of the rectangle
	 */
	Vec3 m_point4;
public:
	/**
	 * Constructor of the rectangle
	 * @param p_color The RGB color of the rectangle
	 * @param p_point1 The first vertex of the rectangle
	 * @param p_point2 The second vertex of the rectangle
	 * @param p_point3 The third vertex of the rectangle
	 * @param p_point4 The 4th vertex of the rectangle
	 * @param mBrillianceAmount The brilliance amount of the rectangle
	 * @param p_reflect_coefficient the reflect coefficient
	 */
	Rectangle(tagRGBQUAD p_color, const Vec3& p_point1, const Vec3& p_point2, const Vec3& p_point3,
		const Vec3& p_point4, double mBrillianceAmount, double p_reflect_coefficient);

	/**
	 * Default constructor (unused)
	 */
	Rectangle();

	/**
	 * Getter of the first vertex position
	 * @return The position of the first vertex
	 */
	[[nodiscard]] const Vec3& getMPoint1() const;

	/**
	 * Getter of the second vertex position
	 * @return The position of the second vertex
	 */
	[[nodiscard]] const Vec3& getMPoint2() const;

	/**
	 * Getter of the third vertex position
	 * @return The position of the third vertex
	 */
	[[nodiscard]] const Vec3& getMPoint3() const;

	/**
	 * Getter of the 4th vertex position
	 * @return The position of the 4th vertex
	 */
	[[nodiscard]] const Vec3& getMPoint4() const;

	/**
	 * Compute the centroid of the rectangle formed by the vertices in parameters
	 * @param p_p1 The first vertex
	 * @param p_p2 The second vertex
	 * @param p_p3 The third vertex
	 * @param p_p4 The 4th vertex
	 * @return The centroid position of the rectangle formed by the four vertices
	 */
	static Vec3 computeCentroid(Vec3 p_p1, Vec3 p_p2, Vec3 p_p3, Vec3 p_p4);

	/**
	 * Destructor of rectangle
	 */
	~Rectangle() override;

	/**
	 * Compute the normal at the vector in parameter for a rectangle
	 * @param p_point The point to compute the normal at
	 * @return The normal vector at the given vector
	 */
	Vec3 getNormalAt(Vec3 p_point) override;
};