#pragma once

#include "Vec3.h"
#include "Triangle.h"
#include "Rectangle.h"

/**
 * Class containing the methods to compute the intersection between a ray and the objects
 */
class IntersectionRayObjectManager
{
public:
	/**
	 * Compute the intersection between a ray and a sphere
	 * @param p_sphereCenter The position of the sphere
	 * @param p_sphereRadius The radius of the sphere
	 * @param p_rayOrigin The origin position of the ray
	 * @param p_rayDirection The direction of the ray
	 * @param p_intersectionCoordinates After the call to the method : Vec3(0,0,0) if no intersection or a Vec3 wth the coordinates of the intersection
	 * @return True if there is an intersection, false if there is not
	 */
	static bool intersectionSphere(const Vec3& p_sphereCenter, float p_sphereRadius, const Vec3& p_rayOrigin, const Vec3& p_rayDirection,
		Vec3& p_intersectionCoordinates);

	/**
	 * Compute the intersection between a ray and a triangle
	 * @param p_trianglePoint1 First vertex of the triangle
	 * @param p_trianglePoint2 Second vertex of the triangle
	 * @param p_trianglePoint3 Third vertex of the triangle
	 * @param p_rayOrigin The origin position of the ray
	 * @param p_rayDirection The direction of the ray
	 * @param p_intersectionCoordinates After the call to the method : Vec3(0,0,0) if no intersection or a Vec3 wth the coordinates of the intersection
	 * @return True if there is an intersection, false if there is not
	 */
	static bool
		intersectionTriangle(const Vec3& p_trianglePoint1, const Vec3& p_trianglePoint2, const Vec3& p_trianglePoint3, const Vec3& p_rayOrigin,
			const Vec3& p_rayDirection, Vec3& p_intersectionCoordinates);

	/**
	 * Compute the intersection between a ray and a triangle
	 * @param p_base Triangle representing the base of the tetrahedron
	 * @param p_face1 Triangle representing the first face of the tetrahedron
	 * @param p_face2 Triangle representing the second face of the tetrahedron
	 * @param p_face3 Triangle representing the third face of the tetrahedron
	 * @param p_rayOrigin The origin position of the ray
	 * @param p_rayDirection The direction of the ray
	 * @param p_intersectionCoordinates After the call to the method : Vec3(0,0,0) if no intersection or a Vec3 wth the coordinates of the intersection
	 * @return True if there is an intersection, false if there is not
	 */
	static bool
		intersectionTetrahedron(const Triangle& p_base, const Triangle& p_face1, const Triangle& p_face2, const Triangle& p_face3, const Vec3& p_rayOrigin,
			const Vec3& p_rayDirection, Vec3& p_intersectionCoordinates);

	/**
	 * Compute the intersection between a ray and a rectangle
	 * @param p_rectanglePoint1 First vertex of the rectangle
	 * @param p_rectanglePoint2 Second vertex of the rectangle
	 * @param p_rectanglePoint3 Third vertex of the rectangle
	 * @param p_rectanglePoint4 4th vertex of the rectangle
	 * @param p_rayOrigin The origin position of the ray
	 * @param p_rayDirection The direction of the ray
	 * @param p_intersectionCoordinates After the call to the method : Vec3(0,0,0) if no intersection or a Vec3 with the coordinates of the intersection
	 * @return True if there is an intersection, false if there is not
	 */
	static bool intersectionRectangle(Vec3 p_rectanglePoint1, Vec3 p_rectanglePoint2, Vec3 p_rectanglePoint3, Vec3 p_rectanglePoint4,
		Vec3 p_rayOrigin, Vec3 p_rayDirection, Vec3& p_intersectionCoordinates);

	/**
	 * Compute the intersection between a ray and a cuboid
	 * @param p_face1 Rectangle representing the first face of the cuboid
	 * @param p_face2 Rectangle representing the second face of the cuboid
	 * @param p_face3 Rectangle representing the third face of the cuboid
	 * @param p_face4 Rectangle representing the 4th face of the cuboid
	 * @param p_face5 Rectangle representing the 5th face of the cuboid
	 * @param p_face6 Rectangle representing the 6th face of the cuboid
	 * @param p_rayOrigin The origin position of the ray
	 * @param p_rayDirection The direction of the ray
	 * @param p_intersectionCoordinates After the call to the method : Vec3(0,0,0) if no intersection or a Vec3 wth the coordinates of the intersection
	 * @return True if there is an intersection, false if there is not
	 */
	static bool intersectionCuboid(Rectangle p_face1, Rectangle p_face2, Rectangle p_face3, Rectangle p_face4, Rectangle p_face5,
		Rectangle p_face6, Vec3 p_rayOrigin, Vec3 p_rayDirection, Vec3& p_intersectionCoordinates);


};
