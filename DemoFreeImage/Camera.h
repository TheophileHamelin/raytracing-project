#pragma once

#include "Vec3.h"

/**
 * Class representing a Camera in the scene
 */
class Camera
{
private:
	/**
	 * The position of the camera in the world
	 */
	Vec3 m_position;

	/**
	 * The base direction of the pointing ray
	 * NOTE : This MUST be normalized
	 */
	Vec3 m_baseDirection;

	/**
	 * The ray that will point towards a point in the projection plane
	 */
	Vec3 m_pointingRay;

	/**
	 * The focal distance, between the camera and the projection plane
	 */
	double m_focalDistance;

	/**
	 * The horizontal field of view of the camera.
	 * NOTE : This MUST be in degrees, this will be converted into radians when making calculations
	 */
	int m_horizontalFOV;

	/**
	 * The vertical field of view of the camera.
	 * NOTE : This MUST be in degrees, this will be converted into radians when making calculations
	 */
	int m_verticalFOV;

	/**
	 * The screen width
	 */
	int m_screenWidth;

	/**
	 * The screen height
	 */
	int m_screenHeight;

	/**
	 * The horizontal scale between the projection plane and the screen
	 */
	double m_screenHorizontalScale;

	/**
	 * The vertical scale between the projection plane and the screen
	 */
	double m_screenVerticalScale;

public:
	/**
	 * Constructor of the camera
	 * @param p_position the position of the camera
	 * @param p_direction the base direction of the camera
	 * @param p_focal the focal distance
	 * @param p_horizontalFOV the horizontal FOV
	 * @param p_verticalFOV the vertical FOV
	 * @param p_screenWidth the screen width
	 * @param p_screenHeight the screen height
	 */
	Camera(const Vec3& p_position, const Vec3& p_direction, double p_focal, int p_horizontalFOV, int p_verticalFOV,
		int p_screenWidth, int p_screenHeight);

	/**
	 * Destructor
	 */
	~Camera();

	/**
	 * Method used to get a normalized ray from a pixel in the screen
	 * @param p_pixelX the X coordinate of the pixel in the screen
	 * @param p_pixelY the Y coordinate of the pixel in the screen
	 * @return a normalized ray pointing towards the correct pixel in the projection plane
	 */
	Vec3 getRayForScreenPixel(int p_pixelX, int p_pixelY);

	/**
	 * getter of the camera position
	 * @return The camera positon
	 */
	[[nodiscard]] const Vec3& getMPosition() const;

};