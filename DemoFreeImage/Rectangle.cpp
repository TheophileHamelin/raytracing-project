#include "Rectangle.h"
#include "MathVector.h"


Rectangle::Rectangle(tagRGBQUAD p_color, const Vec3& p_point1, const Vec3& p_point2, const Vec3& p_point3,
	const Vec3& p_point4, double mBrillianceAmount, double p_reflect_coefficient) : Object(
		Rectangle::computeCentroid(p_point1, p_point2, p_point3, p_point4), p_color, mBrillianceAmount,
		p_reflect_coefficient)
{
	this->m_point1 = p_point1;
	this->m_point2 = p_point2;
	this->m_point3 = p_point3;
	this->m_point4 = p_point4;

	/* Centroid of a rectangle (Centroid = position):
	 * X = (x1 + x2 + x3 + x4)/4
	 * Y = (y1 + y2 + y3 + y4)/4
	 * Z = (z1 + z2 + z3 + z4)/4
	*/
}

Rectangle::~Rectangle() = default;

const Vec3& Rectangle::getMPoint1() const
{
	return m_point1;
}

const Vec3& Rectangle::getMPoint2() const
{
	return m_point2;
}

const Vec3& Rectangle::getMPoint3() const
{
	return m_point3;
}

const Vec3& Rectangle::getMPoint4() const
{
	return m_point4;
}

Rectangle::Rectangle() : Object(Vec3(0, 0, 0), { 0, 0, 0 }, 0.f, 0.0f)
{}

Vec3 Rectangle::computeCentroid(Vec3 p_p1, Vec3 p_p2, Vec3 p_p3, Vec3 p_p4)
{
	double x = (p_p1.getCoordinateX() + p_p2.getCoordinateX() + p_p3.getCoordinateX() + p_p4.getCoordinateX()) / 4.0;
	double y = (p_p1.getCoordinateY() + p_p2.getCoordinateY() + p_p3.getCoordinateY() + p_p4.getCoordinateY()) / 4.0;
	double z = (p_p1.getCoordinateZ() + p_p2.getCoordinateZ() + p_p3.getCoordinateZ() + p_p4.getCoordinateZ()) / 4.0;

	return Vec3(x, y, z);
}

Vec3 Rectangle::getNormalAt(Vec3 p_point)
{
	//only 3 points are necessary to define a plan.
	Vec3 v1 = this->m_point2 - this->m_point1;
	Vec3 v2 = this->m_point3 - this->m_point1;
	return MathVector::multiply(-1.0, MathVector::normalize(MathVector::vectorProduct(v1, v2)));
}