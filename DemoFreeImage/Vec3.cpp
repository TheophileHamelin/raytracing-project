#include "Vec3.h"

Vec3::Vec3(double x, double y, double z)
{
	this->m_x = x;
	this->m_y = y;
	this->m_z = z;
}

double Vec3::getCoordinateX() const
{
	return this->m_x;
}

double Vec3::getCoordinateY() const
{
	return this->m_y;
}

double Vec3::getCoordinateZ() const
{
	return this->m_z;
}

[[maybe_unused]] void Vec3::toString() const
{
	std::cout << "[" << this->m_x << " , " << this->m_y << " , " << this->m_z << "]" << std::endl;
}

Vec3 operator-(const Vec3& v1, const Vec3& v2)
{
	return Vec3(v1.getCoordinateX() - v2.getCoordinateX(), v1.getCoordinateY() - v2.getCoordinateY(),
		v1.getCoordinateZ() - v2.getCoordinateZ());
}

Vec3 operator+(const Vec3& v1, const Vec3& v2)
{
	return Vec3(v1.getCoordinateX() + v2.getCoordinateX(), v1.getCoordinateY() + v2.getCoordinateY(),
		v1.getCoordinateZ() + v2.getCoordinateZ());
}

Vec3 operator/(const Vec3& v1, double d)
{
	return Vec3(v1.getCoordinateX() / d, v1.getCoordinateY() / d, v1.getCoordinateZ() / d);
}

Vec3::~Vec3() = default;

Vec3::Vec3()
{
	this->m_x = 0;
	this->m_y = 0;
	this->m_z = 0;
}
