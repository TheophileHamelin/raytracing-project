#include "Tetrahedron.h"
#include "MathVector.h"

Tetrahedron::Tetrahedron(const tagRGBQUAD& mColor, double pBrillianceAmount, const Vec3& mPoint1,
	const Vec3& mPoint2, const Vec3& mPoint3, const Vec3& mPoint4, double p_reflect_coefficient)
	: Object(
		Tetrahedron::computeCentroid(mPoint1, mPoint2, mPoint3, mPoint4), mColor,
		pBrillianceAmount, p_reflect_coefficient)
{
	this->m_base = Triangle(mColor, mPoint2, mPoint3, mPoint4, pBrillianceAmount, p_reflect_coefficient);
	this->m_face1 = Triangle(mColor, mPoint1, mPoint2, mPoint3, pBrillianceAmount, p_reflect_coefficient);
	this->m_face2 = Triangle(mColor, mPoint1, mPoint3, mPoint4, pBrillianceAmount, p_reflect_coefficient);
	this->m_face3 = Triangle(mColor, mPoint1, mPoint2, mPoint4, pBrillianceAmount, p_reflect_coefficient);

}

const Triangle& Tetrahedron::getMFace1() const
{
	return m_face1;
}

const Triangle& Tetrahedron::getMFace2() const
{
	return m_face2;
}

const Triangle& Tetrahedron::getMFace3() const
{
	return m_face3;
}

const Triangle& Tetrahedron::getMBase() const
{
	return m_base;
}

Tetrahedron::~Tetrahedron() = default;

Vec3 Tetrahedron::computeCentroid(const Vec3 & mPoint1, const Vec3 & mPoint2, const Vec3 & mPoint3, const Vec3 & mPoint4)
{
	double x = (mPoint1.getCoordinateX() + mPoint2.getCoordinateX() + mPoint3.getCoordinateX() +
		mPoint4.getCoordinateX()) / 4.0;
	double y = (mPoint1.getCoordinateY() + mPoint2.getCoordinateY() + mPoint3.getCoordinateY() +
		mPoint4.getCoordinateY()) / 4.0;
	double z = (mPoint1.getCoordinateZ() + mPoint2.getCoordinateZ() + mPoint3.getCoordinateZ() +
		mPoint4.getCoordinateZ()) / 4.0;

	return Vec3(x, y, z);
}

Vec3 Tetrahedron::getNormalAt(Vec3 p_point)
{
	return MathVector::normalize(p_point - this->getMPosition());
}
