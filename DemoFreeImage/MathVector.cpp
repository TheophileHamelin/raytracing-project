#include "MathVector.h"
#include <cmath>

#define M_PI		3.14159265358979323846


double MathVector::norm(const Vec3& v)
{
    return sqrt(v.getCoordinateX() * v.getCoordinateX() + v.getCoordinateY() * v.getCoordinateY() +
        v.getCoordinateZ() * v.getCoordinateZ());
}

double MathVector::scalarProduct(const Vec3& v1, const Vec3& v2)
{
    return v1.getCoordinateX() * v2.getCoordinateX() + v1.getCoordinateY() * v2.getCoordinateY() +
        v1.getCoordinateZ() * v2.getCoordinateZ();
}

Vec3 MathVector::vectorProduct(const Vec3& v1, const Vec3& v2)
{
    double x = v1.getCoordinateY() * v2.getCoordinateZ() - v1.getCoordinateZ() * v2.getCoordinateY();
    double y = v1.getCoordinateZ() * v2.getCoordinateX() - v1.getCoordinateX() * v2.getCoordinateZ();
    double z = v1.getCoordinateX() * v2.getCoordinateY() - v1.getCoordinateY() * v2.getCoordinateX();
    Vec3 result = Vec3(x, y, z);
    return result;
}

Vec3 MathVector::multiply(double i, const Vec3& v)
{
    return Vec3(v.getCoordinateX() * i, v.getCoordinateY() * i, v.getCoordinateZ() * i);
}

Vec3 MathVector::reflectedRay(const Vec3& normal, const Vec3& incident)
{
    Vec3 temp = multiply(2 * scalarProduct(normal, incident), normal);
    return incident - temp;
}

Vec3 MathVector::normalize(const Vec3& v)
{
    double n = MathVector::norm(v);
    return v / n;
}

Vec3 MathVector::median(const Vec3& v1, const Vec3& v2)
{
    return Vec3((v1 + v2) / MathVector::norm(v1 + v2));
}

Vec3 MathVector::normalPlane(const Vec3& v1, const Vec3& v2, const Vec3& v3)
{
    return MathVector::vectorProduct((v2 - v1), (v3 - v1));
}

void MathVector::getUVForVector(const Vec3& v1, double& u, double& v)
{
    double phi = atan2(v1.getCoordinateZ(), v1.getCoordinateX());
    double theta = asin(v1.getCoordinateY());
    u = (phi + M_PI) / (2 * M_PI);
    v = (theta + M_PI / 2) / M_PI;
}

[[maybe_unused]] Vec3 MathVector::refractedRay(const Vec3& normal, const Vec3& incident, double refractionIndex) {
    double scalar = MathVector::scalarProduct(normal, incident);
    Vec3 N = normal;
    //By default, the ray is from the air
    double refractionIndexInitial = 1;
    double refractionIndexFinal = refractionIndex;

    if (scalar < 0) {
        //Ray from outside the medium
        scalar = -scalar;
    }
    else {
        //Ray from inside the medium, we invert the normal
        N = MathVector::multiply(-1.0, N);
        std::swap(refractionIndexInitial, refractionIndexFinal);
    }

    double t = refractionIndexInitial / refractionIndexFinal;

    double k = 1.0 - t * t * (1.0 - scalar * scalar);
    if (k < 0) {
        //No refraction in this case
        return Vec3(0, 0, 0);
    }
    else {
        return MathVector::multiply(t, incident) + MathVector::multiply(t * scalar - sqrt(k), N);
    }
}

