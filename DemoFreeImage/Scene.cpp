#include <cmath>
#include "Scene.h"
#include "IntersectionRayObjectManager.h"
#include "MathVector.h"
#include "Sphere.h"
#include "Tetrahedron.h"
#include "Rectangle.h"
#include "Cuboid.h"

#define PI 3.14159265358979323846
#define TEXTURE_WIDTH 2048
#define TEXTURE_HEIGHT 1024

Scene::Scene(Camera* p_camera, int p_screenWidth, int p_screenHeight)
{
	this->m_camera = p_camera;
	this->m_screenWidth = p_screenWidth;
	this->m_screenHeight = p_screenHeight;

	for (int i = 0; i < p_screenWidth; ++i)
	{
		for (int j = 0; j < p_screenHeight; ++j)
		{
			this->m_screenPixels[i][j].rgbRed = BYTE(255 * 0);
			this->m_screenPixels[i][j].rgbGreen = BYTE(255 * 0);
			this->m_screenPixels[i][j].rgbBlue = BYTE(255 * 0);
		}
	}
}

void Scene::addLight(LightSource* p_light_source)
{
	this->m_light_sources.push_back(p_light_source);
}

void Scene::addObject(Object* p_object)
{
	this->m_objects.push_back(p_object);
}

bool Scene::renderObject(Object* p_object, const Vec3& p_ray, double* p_lowest_distance, unsigned int p_xScreenPixel,
	unsigned int p_yScreenPixel)
{
	Vec3 intersection;

	bool intersectionCheckBool = false;

	//Checking the type of the object to compute the intersection
	if (dynamic_cast<Sphere*>(p_object))
	{
		intersectionCheckBool = IntersectionRayObjectManager::intersectionSphere(p_object->getMPosition(),
			dynamic_cast<Sphere*>(p_object)->getMRadius(),
			this->m_camera->getMPosition(), p_ray,
			intersection);
	}
	else if (dynamic_cast<Triangle*>(p_object))
	{
		intersectionCheckBool = IntersectionRayObjectManager::intersectionTriangle(
			dynamic_cast<Triangle*>(p_object)->getMPoint1(), dynamic_cast<Triangle*>(p_object)->getMPoint2(),
			dynamic_cast<Triangle*>(p_object)->getMPoint3(), this->m_camera->getMPosition(), p_ray, intersection);
	}
	else if (dynamic_cast<Tetrahedron*>(p_object))
	{
		intersectionCheckBool = IntersectionRayObjectManager::intersectionTetrahedron(
			dynamic_cast<Tetrahedron*>(p_object)->getMBase(), dynamic_cast<Tetrahedron*>(p_object)->getMFace1(),
			dynamic_cast<Tetrahedron*>(p_object)->getMFace2(), dynamic_cast<Tetrahedron*>(p_object)->getMFace3(),
			this->m_camera->getMPosition(), p_ray, intersection);
	}
	else if (dynamic_cast<Rectangle*>(p_object))
	{
		intersectionCheckBool = IntersectionRayObjectManager::intersectionRectangle(
			dynamic_cast<Rectangle*>(p_object)->getMPoint1(), dynamic_cast<Rectangle*>(p_object)->getMPoint2(),
			dynamic_cast<Rectangle*>(p_object)->getMPoint3(), dynamic_cast<Rectangle*>(p_object)->getMPoint4(),
			this->m_camera->getMPosition(), p_ray, intersection);
	}
	else if (dynamic_cast<Cuboid*>(p_object))
	{
		intersectionCheckBool = IntersectionRayObjectManager::intersectionCuboid(
			dynamic_cast<Cuboid*>(p_object)->getMFace1(), dynamic_cast<Cuboid*>(p_object)->getMFace2(),
			dynamic_cast<Cuboid*>(p_object)->getMFace3(), dynamic_cast<Cuboid*>(p_object)->getMFace4(),
			dynamic_cast<Cuboid*>(p_object)->getMFace5(), dynamic_cast<Cuboid*>(p_object)->getMFace6(),
			this->m_camera->getMPosition(), p_ray, intersection);
	}
	else
	{
		std::cout << "Chelou" << std::endl;
	}

	//Intersection testing
	if (intersectionCheckBool)
	{
		//Compute the distance between the point and the ray origin
		double distance = MathVector::norm(intersection - m_camera->getMPosition());

		if (distance >= *p_lowest_distance)
		{
			// The point is behind an object : cancelling the rendering
			return false;
		}
		else
		{
			double I_d = 0.0f, I_s = 0.0f, I_a = 1.0f;

			*p_lowest_distance = distance;

			// Enlight the scene
			for (auto& light : this->m_light_sources)
			{
				light->enlight(p_ray, p_object, intersection, this->m_objects, I_d, I_s, I_a);
			}

			I_s *= p_object->getMBrillianceAmount();

			int red, green, blue;

			double u, v;

			// Get UV coordinates for the intersection
			MathVector::getUVForVector(intersection, u, v);
			int i = (u) * (TEXTURE_WIDTH - 1);
			int j = (v) * (TEXTURE_HEIGHT - 1);
			if (i < 0) i = 0;
			if (j < 0) j = 0;
			if (i > TEXTURE_WIDTH - 1) i = TEXTURE_WIDTH - 1;
			if (j > TEXTURE_HEIGHT - 1) j = TEXTURE_HEIGHT - 1;

			// i,j are now a pixel in the texture
			RGBQUAD color;

			// Check the case if the object has a texture or not
			if (p_object->getMTexture() != nullptr)
			{
				FreeImage_GetPixelColor(p_object->getMTexture(), i, j, &color);
			}
			else
			{
				color = p_object->getMColor();
			}

			//Get the reflected vector at the intersection, and add a bias
			Vec3 reflectedVector = MathVector::normalize(
				MathVector::reflectedRay(MathVector::multiply(1.5f, p_object->getNormalAt(intersection)), p_ray));

			Vec3 reflectedIntersection;

			Object* target_object = nullptr;

			for (auto object : this->m_objects)
			{
				if (object == p_object) continue;
				bool reflectionBool = false;

				//Checking the type of the object to compute the intersection
				if (dynamic_cast<Sphere*>(object))
				{
					reflectionBool = IntersectionRayObjectManager::intersectionSphere(object->getMPosition(),
						dynamic_cast<Sphere*>(object)->getMRadius(),
						intersection,
						reflectedVector,
						reflectedIntersection);
				}
				else if (dynamic_cast<Triangle*>(object))
				{
					reflectionBool = IntersectionRayObjectManager::intersectionTriangle(
						dynamic_cast<Triangle*>(object)->getMPoint1(),
						dynamic_cast<Triangle*>(object)->getMPoint2(),
						dynamic_cast<Triangle*>(object)->getMPoint3(), intersection, reflectedVector,
						reflectedIntersection);
				}
				else if (dynamic_cast<Tetrahedron*>(object))
				{
					reflectionBool = IntersectionRayObjectManager::intersectionTetrahedron(
						dynamic_cast<Tetrahedron*>(object)->getMBase(),
						dynamic_cast<Tetrahedron*>(object)->getMFace1(),
						dynamic_cast<Tetrahedron*>(object)->getMFace2(),
						dynamic_cast<Tetrahedron*>(object)->getMFace3(),
						intersection, reflectedVector, reflectedIntersection);
				}

				if (reflectionBool)
				{
					target_object = object;
					break;
				}
			}

			// Compute the color

			auto lux = double(I_a + I_d);
			lux = lux > 1.0f ? 1.0f : lux;

			double coefficient = p_object->getMReflectCoefficient();

			if (target_object != nullptr)
			{
				RGBQUAD targetColor;
				if (target_object->getMTexture() != nullptr)
				{
					FreeImage_GetPixelColor(target_object->getMTexture(), i, j, &targetColor);
				}
				else
				{
					targetColor = target_object->getMColor();
				}

				red = int(color.rgbRed * lux + 255 * I_s + coefficient * targetColor.rgbRed * lux);
				green = int(color.rgbGreen * lux + 255 * I_s + coefficient * targetColor.rgbGreen * lux);
				blue = int(color.rgbBlue * lux + 255 * I_s + coefficient * targetColor.rgbBlue * lux);

			}
			else
			{
				red = int(color.rgbRed * lux + 255 * I_s);
				green = int(color.rgbGreen * lux + 255 * I_s);
				blue = int(color.rgbBlue * lux + 255 * I_s);
			}

			m_screenPixels[p_xScreenPixel][p_yScreenPixel].rgbRed = BYTE(red > 255 ? 255 : red);
			m_screenPixels[p_xScreenPixel][p_yScreenPixel].rgbGreen = BYTE(green > 255 ? 255 : green);
			m_screenPixels[p_xScreenPixel][p_yScreenPixel].rgbBlue = BYTE(blue > 255 ? 255 : blue);

			return true;

		}
	}

	return false;
}

void Scene::save(const char* p_filename)
{
	FIBITMAP* scene_image;
	scene_image = FreeImage_Allocate(this->m_screenWidth, this->m_screenHeight, 32);

	for (int i = 0; i < this->m_screenWidth; i++)
	{
		for (int j = 0; j < this->m_screenHeight; j++)
		{
			FreeImage_SetPixelColor(scene_image, i, j, &this->m_screenPixels[i][j]);
		}
	}

	FreeImage_Save(FIF_BMP, scene_image, p_filename);
}

Scene::~Scene()
{
	delete this->m_camera;
	for (auto light : this->m_light_sources)
	{
		delete light;
	}
	for (auto object : this->m_objects)
	{
		delete object;
	}
}

void Scene::render()
{
	double lowest_distance;
	Vec3 ray;

	for (int i = 0; i < this->m_screenWidth; i++)
	{
		for (int j = 0; j < this->m_screenHeight; j++)
		{
			//Initializing the lowest distance between an object and the ray origin to the max
			lowest_distance = std::numeric_limits<double>::max();
			ray = this->m_camera->getRayForScreenPixel(i, j);

			for (auto& m_object : this->m_objects)
			{
				if (!renderObject(m_object, ray, &lowest_distance, i, j))
				{
					continue;
				}
			}

		}
	}
}

void Scene::blur()
{
	// This is the kernel for the filter
	double GKernel[3][3] = {
			{1, 3,  1},
			{3, 15, 3},
			{1, 3,  1}
	};

	RGBQUAD pix[SCREEN_WIDTH][SCREEN_HEIGHT];

	// Divide the kernel
	for (auto& i : GKernel)
	{
		for (double& j : i)
		{
			j /= 31;
		}
	}

	// Initialize the temp array of pixels
	for (int i = 0; i < this->m_screenWidth; ++i)
	{
		for (int j = 0; j < this->m_screenHeight; ++j)
		{
			pix[i][j].rgbRed = BYTE(255 * 0);
			pix[i][j].rgbGreen = BYTE(255 * 0);
			pix[i][j].rgbBlue = BYTE(255 * 0);
		}
	}

	//Apply the filter to the pixels, and store the resulting pixel into the temp pix array
	for (int i = 0; i < this->m_screenWidth; ++i)
	{
		for (int j = 0; j < this->m_screenHeight; ++j)
		{
			if (i - 2 < 0 || i + 2 > this->m_screenWidth || j - 2 < 0 || j + 2 > this->m_screenHeight) continue;
			double sm = 0;
			for (int k = 0; k < 3; ++k)
			{
				for (int l = 0; l < 3; ++l)
				{
					sm += this->m_screenPixels[i - 1 + k][j - 1 + l].rgbBlue * GKernel[k][l];
				}
			}

			pix[i][j].rgbBlue = sm > 255 ? 255 : sm;

			sm = 0;
			for (int k = 0; k < 3; ++k)
			{
				for (int l = 0; l < 3; ++l)
				{
					sm += this->m_screenPixels[i - 1 + k][j - 1 + l].rgbGreen * GKernel[k][l];
				}
			}
			pix[i][j].rgbGreen = sm > 255 ? 255 : sm;

			sm = 0;
			for (int k = 0; k < 3; ++k)
			{
				for (int l = 0; l < 3; ++l)
				{
					sm += this->m_screenPixels[i - 1 + k][j - 1 + l].rgbRed * GKernel[k][l];
				}
			}
			pix[i][j].rgbRed = sm > 255 ? 255 : sm;
		}
	}

	// Apply the pix array onto the screen pixels
	for (int i = 0; i < this->m_screenWidth; ++i)
	{
		for (int j = 0; j < this->m_screenHeight; ++j)
		{
			this->m_screenPixels[i][j].rgbRed = pix[i][j].rgbRed;
			this->m_screenPixels[i][j].rgbGreen = pix[i][j].rgbGreen;
			this->m_screenPixels[i][j].rgbBlue = pix[i][j].rgbBlue;
		}
	}
}


