#include <iostream>
#include <cmath>
#include "FreeImage.h"
#include "MathVector.h"
#include "Vec3.h"
#include "Camera.h"
#include "Scene.h"
#include "Sphere.h"
#include "Triangle.h"
#include "Tetrahedron.h"


int main(int argc, char** argv)
{
	Camera* cam = new Camera(Vec3(1, 0, -5), Vec3(-0.25f, 0, 1), 3.0f, 107, 90, 640, 480);

	LightSource* lightSource = new LightSource(Vec3(0, 5, 0));

	Sphere* sun = new Sphere(Vec3(0, 0, 0), { 255, 0, 0, 0 }, 1.0f, 0.0f, 0.0f);
	Sphere* mercury = new Sphere(Vec3(2, 0, 0), { 255, 0, 0, 0 }, 0.25f, 0.0f, 0.0f);
	Sphere* venus = new Sphere(Vec3(0, 0, -3), { 255, 0, 0, 0 }, 0.25f, 0.0f, 0.0f);
	Sphere* earth = new Sphere(Vec3(-4, 0, 0), { 255, 0, 0, 0 }, 0.25f, 0.0f, 0.0f);
	Sphere* mars = new Sphere(Vec3(0, 0, 5), { 255, 0, 0, 0 }, 0.25f, 0.0f, 0.0f);
	Sphere* jupiter = new Sphere(Vec3(6, 0, 6), { 255, 0, 0, 0 }, 0.5f, 0.0f, 0.0f);
	Sphere* saturn = new Sphere(Vec3(8, 0, -8), { 255, 0, 0, 0 }, 0.5f, 0.0f, 0.0f);
	Sphere* uranus = new Sphere(Vec3(-9, 0, -9), { 255, 0, 0, 0 }, 0.4f, 0.0f, 0.0f);
	Sphere* neptune = new Sphere(Vec3(-10, 0, 10), { 255, 0, 0, 0 }, 0.4f, 0.0f, 0.0f);



	//FIBITMAP *texture = FreeImage_Load(FIF_JPEG, "../Marble_Blue_004_basecolor.jpg");
	FIBITMAP* sunTexture = FreeImage_Load(FIF_JPEG, "textures/sun.jpg");
	FIBITMAP* mercuryTexture = FreeImage_Load(FIF_JPEG, "textures/mercury.jpg");
	FIBITMAP* venusTexture = FreeImage_Load(FIF_JPEG, "textures/venus.jpg");
	FIBITMAP* earthTexture = FreeImage_Load(FIF_JPEG, "textures/earth.jpg");
	FIBITMAP* marsTexture = FreeImage_Load(FIF_JPEG, "textures/mars.jpg");
	FIBITMAP* jupiterTexture = FreeImage_Load(FIF_JPEG, "textures/jupiter.jpg");
	FIBITMAP* saturnTexture = FreeImage_Load(FIF_JPEG, "textures/saturn.jpg");
	FIBITMAP* uranusTexture = FreeImage_Load(FIF_JPEG, "textures/uranus.jpg");
	FIBITMAP* neptuneTexture = FreeImage_Load(FIF_JPEG, "textures/neptune.jpg");

	//FIBITMAP *texture = FreeImage_Load(FIF_JPEG, "../Marble.jpg");

	sun->setMTexture(sunTexture);
	mercury->setMTexture(mercuryTexture);
	venus->setMTexture(venusTexture);
	earth->setMTexture(earthTexture);
	mars->setMTexture(marsTexture);
	jupiter->setMTexture(jupiterTexture);
	saturn->setMTexture(saturnTexture);
	uranus->setMTexture(uranusTexture);
	neptune->setMTexture(neptuneTexture);

	Scene* sc = new Scene(cam, 640, 480);
	sc->addObject(sun);
	sc->addObject(mercury);
	sc->addObject(venus);
	sc->addObject(earth);
	sc->addObject(mars);
	sc->addObject(jupiter);
	sc->addObject(saturn);
	sc->addObject(uranus);
	sc->addObject(neptune);
	sc->addLight(lightSource);
	sc->render();
	//sc->blur();
	sc->save("out.bmp");

	delete sc;
	return EXIT_SUCCESS;
}