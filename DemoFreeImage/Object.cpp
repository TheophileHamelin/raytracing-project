#include "Object.h"

Object::Object(const Vec3& mPosition, tagRGBQUAD mColor, double p_brilliance_amount, double p_reflect_coefficient)
	: m_position(
		mPosition), m_color(mColor), m_brilliance_amount(p_brilliance_amount), m_texture(nullptr),
	m_reflect_coefficient(p_reflect_coefficient)
{}

const Vec3& Object::getMPosition() const
{
	return m_position;
}

const RGBQUAD& Object::getMColor() const
{
	return m_color;
}

Object::~Object() = default;


double Object::getMBrillianceAmount() const
{
	return m_brilliance_amount;
}

FIBITMAP* Object::getMTexture() const
{
	return m_texture;
}

void Object::setMTexture(FIBITMAP * mTexture)
{
	m_texture = mTexture;
}

double Object::getMReflectCoefficient() const
{
	return m_reflect_coefficient;
}
